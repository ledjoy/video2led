//imports
const fs = require('fs');
const P2J = require('pipe2jpeg');
const xml2js = require('xml2js');
const jpeg = require('jpeg-js');
const argv = require('minimist')(process.argv.slice(2));
//global settings
const sectorSize = 512; //flash sector size
const tableElementSize = 8; //two 32bit numbers
const stringSize = 16; //size of string settings element
const firstDataSector = 3;
//command line arguments
const config = argv['c'];
const output = argv['o'];

let bytesPerFrame = 0;
let settings = {};
let leds = [];
let movies = [];
let scale = '';
let rgbOrder = {};

let buffers = { //binary data will be here
  settings: Buffer.alloc(512),
  table: Buffer.alloc(1024),
  movies: [],
};

function readSettings() {
  var parser = new xml2js.Parser();
  return new Promise(resolve => {
    fs.readFile(config, function (err, data) {
      parser.parseString(data, function (err, result) {
        settings['ledsPerFrame'] = result.config.layout[0].led.length;
        for (let i in result.config.$)
          settings[i] = result.config.$[i];
        for (let i = 0; i < result.config.layout[0].led.length; i++) {
          leds.push(result.config.layout[0].led[i].$);
        }
        for (let i = 0; i < result.config.movies[0].movie.length; i++)
          movies.push(result.config.movies[0].movie[i].$);
        scale = result.config.layout[0].$['scale'];
        let order = result.config.layout[0].$['order'];
        rgbOrder['r'] = order.indexOf('r');
        rgbOrder['g'] = order.indexOf('g');
        rgbOrder['b'] = order.indexOf('b');
        resolve();
      });
    });
  });
}

function allocBuffers() {
  bytesPerFrame = settings.ledsPerFrame * settings.bytesPerLed;
  let movieOffset = firstDataSector;
  for (let i = 0; i < movies.length; i++) {
    let size = bytesPerFrame * movies[i].lengthInFrames;
    let sectorsPerMovie = Math.ceil(size / sectorSize);
    buffers.movies[i] = Buffer.alloc(sectorsPerMovie * sectorSize);
    let tableElementOffset = tableElementSize * movies[i].note;
    buffers.table.writeUInt32LE(movieOffset, tableElementOffset);
    buffers.table.writeUInt32LE(movies[i].lengthInFrames, tableElementOffset + 4);
    //	console.log("movie #"+i+", allocated "+size+" bytes. First sector: "+movieOffset+", length: "+movies[i].lengthInFrames);
    movies[i].offset = movieOffset;
    movieOffset += sectorsPerMovie;
  }
}

function exportData() {
  let settingsOffset = 0;
  //prepare settings in buffer
  for (let key in settings) {
    switch (typeof settings[key]) {
    case 'number':
      buffers.settings.writeUInt32LE(settings[key], settingsOffset);
      settingsOffset += 4;  //size of UInt32
      break;
    case 'string':
      buffers.settings.write(settings[key], settingsOffset, stringSize);
      settingsOffset += stringSize;
      break;
    }
  }

  var wstream = fs.createWriteStream(output);
  wstream.write(buffers.settings); //write settings
  wstream.write(buffers.table); //write table
  for (let i = 0; i < movies.length; i++) //write movies data
    wstream.write(buffers.movies[i]);
  wstream.end();
}

function processFrame(data, buffer, frame) {
  var rawImageData = jpeg.decode(data);
  for (let led = 0; led < settings.ledsPerFrame; led++) {
    let readOffset = parseInt(leds[led].y) * rawImageData.width + parseInt(leds[led].x);
    let writeOffset = frame * bytesPerFrame + led * settings.bytesPerLed;
    let r = rawImageData.data.readUInt8(readOffset + 0);
    let g = rawImageData.data.readUInt8(readOffset + 1);
    let b = rawImageData.data.readUInt8(readOffset + 2);
    buffer.writeUInt8(r, writeOffset + rgbOrder['r']);
    buffer.writeUInt8(g, writeOffset + rgbOrder['g']);
    buffer.writeUInt8(b, writeOffset + rgbOrder['b']);
  }
}

function processMovie(movie) {
  return new Promise(resolve => {
    var frame = 0;
    var p2j = new P2J();
    var args = [
      '-loglevel', 'quiet',
      '-re',
      '-i', movie.src,
      '-an',
      '-c:v', 'mjpeg',
      '-vframes', movie.lengthInFrames,
      '-pix_fmt', 'yuvj422p',
      '-f', 'image2pipe',
      '-vf', 'fps='+settings.framesPerSecond+',scale=' + scale,
      '-q', '1',
      'pipe:1'
    ];

    p2j.on('jpeg', data => processFrame(data, buffers.movies[movie.note], frame++));
    var ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
    var spawn = require('child_process').spawn;
    var ffmpeg = spawn(ffmpegPath, args, {
      stdio: ['ignore', 'pipe', 'ignore']
    });
    ffmpeg.on('error', (error) => {
      console.log(error);
      resolve();
    });
    ffmpeg.on('exit', () => {
      resolve();
    });
    ffmpeg.stdout.pipe(p2j);
  });
}

readSettings().then(() => {
  allocBuffers();
  Promise.all(movies.map(processMovie)).then(exportData);
});